#include <thread>
#include <chrono>
#include <sys/stat.h>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <novatel_oem7_msgs/INSPVA.h>

#include <opencv2/opencv.hpp>
#define WINNAME "Spainnaker"
class filesave
{
private:
    /* data */
    std::string projectdir;
    std::string cameradir;
    std::string trackdir;
    FILE *insfile;
    double lat, lng;

public:
    filesave(/* args */) : insfile(0)
    {
        int64_t unixtime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        projectdir = std::string(getenv("HOME")) + "/Data/" + std::to_string(unixtime);

        cameradir = projectdir + "/Camera/";
        trackdir = projectdir + "/Imu/";
        _mkdir_p(cameradir.c_str());
        _mkdir_p(trackdir.c_str());

        trackdir = trackdir + "realtime_inspva.txt";
        insfile = fopen(trackdir.c_str(), "wt");
        if (!insfile)
        {
            fprintf(stderr, "文件打开失败 %s\n", trackdir.c_str());
            std::abort();
        }
        fprintf(stderr, "工程开始 %s\n", projectdir.c_str());

        fprintf(insfile, "gpstime(ms) unixtime(ms) latitude longitude height nv ev uv roll pitch yaw\n");
        fflush(insfile);
        cv::namedWindow(WINNAME, cv::WINDOW_NORMAL);
    }

    ~filesave()
    {
        close();
    }

    void close()
    {
        if (insfile)
        {
            fclose(insfile);
            insfile = 0;
            fprintf(stderr, "工程结束 %s\n", projectdir.c_str());
        }
    }

    inline int _mkdir_p(const char *dir)
    {
        if (access(dir, 0) == 0)
        {
            return EEXIST;
        }
        char tmp[512] = {0};
        strncpy(tmp, dir, sizeof(tmp));
        char *p = tmp;
        char delim = '/';
        while (*p)
        {
            if (*p == '/')
            {
                *p = '\0';
                mkdir(tmp, 0777);
                *p = delim;
            }
            ++p;
        }
        return 0;
    }

    void onImage(const sensor_msgs::ImageConstPtr &msg)
    {
        int64_t unixtime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

        cv_bridge::CvImagePtr cv_ptr_compressed = cv_bridge::toCvCopy(msg, msg->encoding);
        cv::Mat cv_ptr = cv_ptr_compressed->image;

        std::string filename = cameradir + std::to_string(unixtime) + ".jpg";
        cv::imwrite(filename, cv_ptr, {cv::IMWRITE_JPEG_QUALITY, 70});

        cv::resizeWindow(WINNAME, cv::Size(960, 540));
        cv::imshow(WINNAME, cv_ptr);
        cv::waitKey(10);

        fprintf(stderr, "%.8f,%.8f\n", lat, lng);
    }

    void onInspva(const novatel_oem7_msgs::INSPVAConstPtr &msg)
    {
        int64_t unixtime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        lat = msg->latitude;
        lng = msg->longitude;
        if (insfile)
        {
            fprintf(insfile, "%10d %ld %.8f %.8f %.3f %.2f %.2f %.2f %.4f %.4f %.4f\n",
                    msg->nov_header.gps_week_milliseconds, unixtime,
                    msg->latitude, msg->longitude, msg->height,
                    msg->north_velocity, msg->east_velocity, msg->up_velocity,
                    msg->roll, msg->pitch, msg->azimuth);
        }
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "rosaver_node");

    filesave fs;

    ros::Subscriber m_pcd;

    ros::NodeHandle nh;
    image_transport::ImageTransport it_(nh);
    image_transport::Subscriber sub_Image = it_.subscribe("/camera/image_color", 10, &filesave::onImage, &fs);

    ros::Subscriber sub_Ins = nh.subscribe("/novatel/oem7/inspva", 100, &filesave::onInspva, &fs);

    ros::spin();
    fs.close();

    return 0;
}