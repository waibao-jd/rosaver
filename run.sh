#!/bin/bash

killall rosaver_node
killall spinnaker
killall novatel

source /opt/ros/melodic/setup.bash
roslaunch novatel_oem7_driver oem7_tty.launch oem7_tty_name:=/dev/ttyUSB0 &
sleep 3
echo " novatel_oem7_driver  starting success!"

sudo systemctl start rtk.service

cd /home/cicvdata/road_map_on_vehicle_fusion_show_plus_plus/road_map_fusion_ros/ROS/

source devel/setup.bash
roslaunch spinnaker_camera_driver camera.launch  &
sleep 3
echo " spinnaker_camera_driver  starting success!"

source devel/setup.bash
roslaunch rosaver rosaver.launch